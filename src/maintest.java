/**
 * Created by Taavi on 21.02.2017.
 */
public class maintest {

    public static void main(String[] args) {
        int[] a = new int[10];
        a[0] = 52;
        a[1] = 5;
        a[2] = 3;
        a[3] = 6;
        a[4] = 5;
        a[5] = 77;
        a[6] = 8;
        a[7] = 44;
        a[8] = 1;
        a[9] = 2;

        binaryInsertionSort(a);

        for (int i = 0; i < a.length; i++)
        {
            System.out.println(i+ "   " + a[i]);
        }

    }

    public static void binaryInsertionSort(int[] a) {
        // TODO!!! Your method here!

        for (int i = 1; i < a.length; i++)
        {
            int left = 0;    // vasakpoolne otspunkt
            int right = i;   // parempoolne otspunkt
            int mid = (left + right) / 2;
            int selected = a[i];

            while (left < right) {

                if (a[i] < a[mid]) {
                    right = mid;
                } else if (a[i] > a[mid]) {
                    left = mid + 1;
                } else {
                    break;
                }

                mid = (left + right) / 2;

                System.out.println("1 mid "+mid);
                System.out.println("1 i " +i);

            }// while end

            if (mid < i) {
                System.out.println("2 mid "+mid);
                System.out.println("i " +i);
                System.arraycopy(a, 0, a, 1, i);
                a[mid] = selected;
            }
        }
    } // otsi lopp
}